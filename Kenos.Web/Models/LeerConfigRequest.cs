﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kenos.Web.Models
{
    public class LeerConfigRequest : AutenticacionModel
    {
        public string NombreInstancia { get; set; }
    }
}