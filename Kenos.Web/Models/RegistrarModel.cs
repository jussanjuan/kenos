﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kenos.Web.Models
{
    public class RegistrarModel
    {
        public string Path { get; set; }
        public string Version { get; set; }
        public string MachineName { get; set; }
        public string AssemblyName { get; set; }
    }
}