﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kenos.Win
{
    public class StreamingTypes
    {
        public const string Audio = "audio";
        public const string Video= "video";
        public const string AudioVideo = "audio-video";
    }
}
