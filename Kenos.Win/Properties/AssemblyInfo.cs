﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Kenos.WinTvGrabber")]
[assembly: AssemblyDescription("Grabación de audiencias")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Poder Judicial del Chubut - Dirección de Registros Judiciales")]
[assembly: AssemblyProduct("Kenos.WinTvGrabber")]
[assembly: AssemblyCopyright("Copyright ©  2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("f95a4f83-e015-41ab-99ca-a67e8cc03ca7")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.1.2.05")]
[assembly: AssemblyFileVersion("2.1.2.05")]
