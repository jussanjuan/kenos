﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kenos.Logger
{
    public enum LogLevel
    {
        Error = 1, 
		Informacion = 2, 
        Debug = 3
    }
}
