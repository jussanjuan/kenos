﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kenos.Common
{
    public class MarcaTiempo
    {
        public TimeSpan Tiempo { get; set; }
        public string Descripcion { get; set; }
    }
}
