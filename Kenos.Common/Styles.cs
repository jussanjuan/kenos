﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kenos.Common
{
    public class Styles
    {
        public static System.Drawing.Color ColorFondoTerciario { get { return System.Drawing.Color.FromArgb(50, 68, 80); } }

        public static System.Drawing.Color ColorFondoSecundario { get { return System.Drawing.Color.FromArgb(194, 195, 201); } }

        public static System.Drawing.Color ColorFondoPrimario { get { return System.Drawing.Color.FromArgb(71, 71, 72); } }

        public static System.Drawing.Color ColorFuentePrimario { get { return ColorFondoSecundario; } }

        public static System.Drawing.Color ColorFuenteSecundario { get { return System.Drawing.Color.FromArgb(62, 61, 59); } }

    }
}
