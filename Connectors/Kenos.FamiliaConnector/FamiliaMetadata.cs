﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kenos.FamiliaConnector
{
    public class FamiliaMetadata : Kenos.Common.Metadata
    {
        public int Numero { get; set; }
        public int Anio { get; set; }
    }
}
